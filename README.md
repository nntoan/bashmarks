### Bashmarks is a shell script that allows you to save and jump to commonly used directories. Now supports tab completion.

## Install

1. `git clone git@bitbucket.org:nntoan/bashmarks.git`
2. `cd bashmarks && make install`
3. `nano ~/.bashrc` or `nano ~/.profile` (OS X) and add these lines 
```
#!bash

# Bashmarks shortcuts
alias s='bm -a'
alias g='bm -g'
alias p='bm -p'
alias d='bm -d'

# Update bashmark file
source ~/.local/bin/bashmarks.sh
```

## Shell Commands

    bm -a <bookmark_name> - Saves the current directory as "bookmark_name"
    bm -g <bookmark_name> - Goes (cd) to the directory associated with "bookmark_name"
    bm -p <bookmark_name> - Prints the directory associated with "bookmark_name"
    bm -d <bookmark_name> - Deletes the bookmark
    bm -l                 - Lists all available bookmarks
    
## Example Usage

    $ cd /var/www/
    $ bm -a webfolder
    $ cd /usr/local/lib/
    $ bm -a locallib
    $ bm -l
    $ bm -g web<tab>
    $ bm -g webfolder

## Where Bashmarks are stored
    
All of your directory bookmarks are saved in a file called ".sdirs" in your HOME directory.
